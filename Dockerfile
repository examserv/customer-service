FROM golang:1.19-alpine
RUN mkdir customer-servie
COPY . /customer-servie
WORKDIR /customer-servie
RUN go mod tidy
RUN go mod vendor
RUN go build -o main cmd/main.go
CMD ./main
EXPOSE 1111