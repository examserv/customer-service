CREATE TABLE IF NOT EXISTS addresses(
    id serial primary key,
    street text,
    district text,
    user_id uuid NOT NULL REFERENCES customers(id),
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    deleted_at TIMESTAMP 
);
