package postgres

import (
	pb "customer-service/genproto/customer"
	"database/sql"
	"fmt"
)

func (r *CustomerRepo) GetModerator(req *pb.GetModeratorReq) (*pb.GetModeratorRes, error) {
	res := pb.GetModeratorRes{}
	err := r.Db.QueryRow(`SELECT 
		id, 
		name, 
		password,
		email,
		created_at,
		updated_at
		FROM moderator
		WHERE deleted_at IS NULL AND name=$1`, req.Name).Scan(
		&res.Id,
		&res.Name,
		&res.Password,
		&res.Email,
		&res.CreatedAt,
		&res.UpdatedAt)

	if err == sql.ErrNoRows {
		fmt.Println("Error while getting admin no rows")
		return &res, nil
	}
	if err != nil {
		fmt.Println("error while getting moderator")
		return &pb.GetModeratorRes{}, err
	}
	return &res, nil
}
