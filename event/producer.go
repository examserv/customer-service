package event

import (
	"context"
	"customer-service/config"
	"customer-service/pkg/logger"
	messagebroker "customer-service/pkg/messageBroker"

	"fmt"
	"time"

	kafka "github.com/segmentio/kafka-go"
)

type KafkaProducer struct {
	kafkaWriter *kafka.Writer
	log         logger.Logger
}

func NewKafkaProducerBroker(cfg config.Config, log logger.Logger, topic string) messagebroker.Producer {
	connstring := fmt.Sprintf("%s:%d", cfg.KafkaHost, cfg.KafkaPort)
	return &KafkaProducer{
		kafkaWriter: &kafka.Writer{
			Addr:         kafka.TCP(connstring),
			Topic:        topic,
			BatchTimeout: 10 * time.Millisecond,
		},
		log: log,
	}
}

func (k *KafkaProducer) Stop() error {
	err := k.kafkaWriter.Close()
	if err != nil {
		return err
	}
	return nil
}
func (k *KafkaProducer) Publish(key, body []byte, logBody string) error {
	message := kafka.Message{
		Key:   key,
		Value: body,
	}
	if err := k.kafkaWriter.WriteMessages(context.Background(), message); err != nil {
		return err
	}
	// k.log.Info("Messsage published(key/body): " + string(key) + "/" + logBody)
	return nil
}

func (k *KafkaProducer) Start() error {
	return nil
}
