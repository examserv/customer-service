package service

import (
	"context"
	pb "customer-service/genproto/customer"
	ps "customer-service/genproto/post"
	"customer-service/pkg/logger"
	l "customer-service/pkg/logger"
	"fmt"

	messagebroker "customer-service/message_broker"
	grpcclient "customer-service/service/grpc_client"
	"customer-service/storage"

	"github.com/jmoiron/sqlx"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type CustomerService struct {
	Storage  storage.IStorage
	Logger   l.Logger
	client   grpcclient.GrpcClientI
	producer *messagebroker.Producer
}

func NewCustomerService(db *sqlx.DB, l l.Logger, client grpcclient.GrpcClientI, producer *messagebroker.Producer) *CustomerService {
	return &CustomerService{
		Storage:  storage.NewStoragePg(db),
		Logger:   l,
		client:   client,
		producer: producer,
	}
}

func (s *CustomerService) CreateCustomer(ctx context.Context, req *pb.CustomerReq) (*pb.CustomerRes, error) {
	res, err := s.Storage.Customer().CreateCustomer(req)

	if err != nil {
		s.Logger.Error("Error while inserting into customer", logger.Error(err))
		// return &pb.CustomerRes{}, status.Error(codes.Internal, "Something went wrong, Please chech user info")
		return &pb.CustomerRes{}, err
	}
	fmt.Println(res)
	// err = s.PublishUserMessage(res)
	for _, ps := range req.Posts {
		p := &pb.Post{
			Id:          ps.Id,
			Name:        ps.Name,
			UserId:      res.Id,
			Description: ps.Description,
		}
		res.Posts = append(res.Posts, p)
	}
	s.producer.ProducerCreate(&pb.CustomerReq{
		Posts: res.Posts,
	})
	if err != nil {
		s.Logger.Error("Error while publishing usuer info", logger.Error(err))
		return &pb.CustomerRes{}, status.Error(codes.Internal, "Error while publishing usuer info")
	}
	return res, nil
}
func (s *CustomerService) GetCustomer(ctx context.Context, req *pb.CustomerId) (*pb.Customer, error) {
	res, err := s.Storage.Customer().GetCustomer(req)
	if err != nil {
		s.Logger.Error("ERROR WHILE GETTING Customer", l.Any("get", err))
		return &pb.Customer{}, status.Error(codes.NotFound, "Some of the users you ask are not exist")
	}
	post, err := s.client.Post().GetPostUserId(ctx, &ps.PostUserId{UserId: req.Id})
	if err != nil {
		s.Logger.Error("error select", l.Any("Error select customer post", err))
		return &pb.Customer{}, status.Error(codes.Internal, "something went wrong, please check customer post")
	}
	for _, ps := range post.Posts {
		p := &pb.Post{
			Id:          ps.Id,
			Name:        ps.Name,
			UserId:      ps.UserId,
			Description: ps.Description,
		}
		for _, media := range ps.Medias {
			p.Medias = append(p.Medias, &pb.Media{
				Id:     media.Id,
				PostId: media.PostId,
				Name:   media.Name,
				Link:   media.Link,
				Type:   media.Type,
			})
		}
		for _, rev := range ps.Reviews {
			p.Reviews = append(p.Reviews, &pb.Review{
				Id:          rev.Id,
				Name:        rev.Name,
				PostId:      rev.PostId,
				UserId:      rev.UserId,
				Description: rev.Description,
				Rating:      rev.Rating,
			})
		}
		res.Posts = append(res.Posts, p)
	}
	return res, nil
}

func (s *CustomerService) UpdateCustomer(ctx context.Context, req *pb.CustomerRes) (*pb.CustomerRes, error) {
	res, err := s.Storage.Customer().UpdateCustomer(req)
	if err != nil {
		s.Logger.Error("Error while updating", logger.Any("Update", err))
		return &pb.CustomerRes{}, status.Error(codes.InvalidArgument, "Please recheck customer info")
	}
	return res, nil
}

func (s *CustomerService) DeleteCustomer(ctx context.Context, id *pb.CustomerId) (*pb.Empty, error) {
	_, err := s.client.Post().DeletePostUserID(ctx, &ps.PostUserId{UserId: id.Id})
	fmt.Println("Error customer service: ", err)
	if err != nil {
		s.Logger.Error("Error while deleteing one user", logger.Any("delete", err))
		return &pb.Empty{}, status.Error(codes.Internal, "Please check, some user that you are gonna delete not exists")
	}
	_, err = s.Storage.Customer().DeleteCustomer(id)
	if err != nil {
		s.Logger.Error("Error while deleteing one user", logger.Any("delete", err))
		return &pb.Empty{}, status.Error(codes.Internal, "Please check, some user that you are gonna delete not exists")
	}
	return &pb.Empty{}, nil
}

func (s *CustomerService) GetCustomers(ctx context.Context, em *pb.Empty) (*pb.Customers, error) {
	customers, err := s.Storage.Customer().GetCustomers(em)
	if err != nil {
		s.Logger.Error("ERROR WHILE GETTING Customers", logger.Any("get", err))
		return &pb.Customers{}, status.Error(codes.NotFound, "Some of the customers you ask are not exist")
	}
	// fmt.Println(customers, "bosh keldi srv ")
	return customers, nil
}

func (s *CustomerService) CheckField(ctx context.Context, req *pb.CheckFieldReq) (*pb.CheckFieldRes, error) {
	res, err := s.Storage.Customer().CheckField(req)
	if err != nil {
		s.Logger.Error("Error while checking field", logger.Any("Select", err))
		return nil, status.Error(codes.Internal, "Please check your data")
	}
	return res, nil

}

func (s *CustomerService) ListCustomers(ctx context.Context, req *pb.ListUserReq) (*pb.Customers, error) {
	res, err := s.Storage.Customer().ListCustomers(req)
	if err != nil {
		s.Logger.Error("Error while getting all lists ", logger.Any("Select", err))
		return nil, status.Error(codes.Internal, "Please check your data")
	}
	return res, nil
}
func (s *CustomerService) SearchCustomer(ctx context.Context, req *pb.SearchReq) (*pb.Customers, error) {
	res, err := s.Storage.Customer().SearchCustomer(req)
	if err != nil {
		s.Logger.Error("Error while getting all lists ", logger.Any("Select", err))
		return nil, status.Error(codes.Internal, "Please check your data")
	}
	return res, nil
}
func (s *CustomerService) LogIn(ctx context.Context, req *pb.LoginRequest) (*pb.CustomerRes, error) {
	res, err := s.Storage.Customer().LogIn(req)
	fmt.Println("_+_+_+_+_+_+_+_+_+", err)
	if err != nil {
		s.Logger.Error("Error while into by login ", logger.Any("Select", err))
		return nil, status.Error(codes.Internal, "Please check your data")
	}
	return res, nil
}
func (s *CustomerService) GetAdmin(ctx context.Context, req *pb.AdminCutomerReq) (*pb.AdminCustomerRes, error) {
	res, err := s.Storage.Customer().GetAdmin(req)

	if err != nil {
		s.Logger.Error("Error while get admin ", logger.Any("Select", err))
		return nil, status.Error(codes.Internal, "Please check your data")
	}
	return res, nil
}
