package main

import (
	"customer-service/config"
	pc "customer-service/genproto/customer"
	messagebroker "customer-service/message_broker"
	"customer-service/pkg/db"
	"customer-service/pkg/logger"
	"customer-service/service"
	grpcclient "customer-service/service/grpc_client"
	"fmt"
	"net"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func main() {
	cfg := config.Load()

	log := logger.New(cfg.LogLevel, "customer-service")
	defer logger.CleanUp(log)

	log.Info("main sqlxConfig",
		logger.String("host", cfg.PostgresHost),
		logger.Int("port", cfg.PostgresPort),
		logger.String("database", cfg.PostgresDatabase),
	)
	connDb, err := db.ConnectToDb(cfg)
	if err != nil {
		log.Fatal("Error while connecting to database", logger.Error(err))
	}
	grpcClient, err := grpcclient.New(cfg)
	if err != nil {
		log.Fatal("grpc connect to client error", logger.Error(err))
	}
	// KAFKA
	userProducer, err := messagebroker.NewProducer(cfg)
	if err != nil {
		fmt.Println(err)
	}
	// KAFKA END

	customerService := service.NewCustomerService(connDb, log, grpcClient, userProducer)

	listen, err := net.Listen("tcp", cfg.RPCHost+":"+cfg.RPCPort)
	if err != nil {
		log.Fatal("Error while lisstening 1: %v", logger.Error(err))
	}

	c := grpc.NewServer()
	reflection.Register(c)
	pc.RegisterCustomerServiceServer(c, customerService)
	log.Info("Server is runnig ", logger.String("port", cfg.RPCPort))
	if err := c.Serve(listen); err != nil {
		log.Fatal("Error while listening: %v", logger.Error(err))
	}
}
