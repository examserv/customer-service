package postgres

import (
	pc "customer-service/genproto/customer"
	"fmt"

	"github.com/jmoiron/sqlx"
)

type CustomerRepo struct {
	Db *sqlx.DB
}

func NewCustomerRepo(db *sqlx.DB) *CustomerRepo {
	return &CustomerRepo{Db: db}
}

func (r *CustomerRepo) CreateCustomer(user *pc.CustomerReq) (*pc.CustomerRes, error) {
	trx, err := r.Db.Begin()
	if err != nil {
		fmt.Println("error while inserting new customer by transaction")
		return &pc.CustomerRes{}, err
	}
	defer trx.Rollback()
	customer := pc.CustomerRes{}
	err = r.Db.QueryRow(`insert into customers
		(id, first_name,last_name,bio,email,phone_number, password, refresh_token)
		values($1, $2, $3, $4, $5, $6, $7, $8) returning id,first_name,last_name,bio,email,phone_number, password, refresh_token`,
		user.Id, user.FirstName, user.LastName, user.Bio, user.Email, user.PhoneNumber, user.Password, user.RefreshToken,
	).Scan(
		&customer.Id,
		&customer.FirstName,
		&customer.LastName,
		&customer.Bio,
		&customer.Email,
		&customer.PhoneNumber,
		&customer.Password,
		&customer.RefreshToken,
	)
	if err != nil {
		fmt.Println("error while inserting customers", err)
		return &pc.CustomerRes{}, err
	}
	for _, address := range user.Addresses {
		addressResp := pc.AddressRes{}
		err := r.Db.QueryRow(`insert into addresses(
			user_id, 
			district, 
			street) values($1, $2, $3) returning user_id, district, street`,
			customer.Id,
			address.District,
			address.Street).Scan(&addressResp.UserId, &addressResp.District, &addressResp.Street)
		if err != nil {
			fmt.Println("error while inserting addresses")
			return &pc.CustomerRes{}, err
		}
		customer.Addresses = append(customer.Addresses, &addressResp)
	}
	trx.Commit()
	return &customer, nil
}

func (r *CustomerRepo) GetCustomers(em *pc.Empty) (*pc.Customers, error) {
	response := &pc.Customers{}
	rows, err := r.Db.Query(`select 
			id,
			first_name,
			last_name,
			bio,
			email,
			phone_number
			from customers where deleted_at is null`)
	if err != nil {
		fmt.Println(err)
		return &pc.Customers{}, err
	}
	for rows.Next() {
		customer := &pc.CustomerRes{}
		err := rows.Scan(
			&customer.Id,
			&customer.FirstName,
			&customer.LastName,
			&customer.Bio,
			&customer.Email,
			&customer.PhoneNumber)
		if err != nil {
			fmt.Println(err)
			return &pc.Customers{}, err
		}
		rowAdd, err := r.Db.Query(`select id, street, district, user_id from addresses where user_id=$1`, customer.Id)
		if err != nil {
			fmt.Println(err)
			return &pc.Customers{}, err
		}
		for rowAdd.Next() {
			address := &pc.AddressRes{}
			err := rowAdd.Scan(
				&address.Id,
				&address.Street,
				&address.District,
				&address.UserId)
			if err != nil {
				fmt.Println(err)
				return &pc.Customers{}, err
			}
			customer.Addresses = append(customer.Addresses, address)
		}
		// fmt.Println(customer)
		response.Customers = append(response.Customers, customer)
	}
	if err != nil {
		fmt.Println(err)
		return &pc.Customers{}, err
	}
	// fmt.Println(response, "bosh keldi str psql")
	return response, nil
}

func (r *CustomerRepo) ListCustomers(req *pc.ListUserReq) (*pc.Customers, error) {
	limit := req.Limit
	page := (req.Page - 1) * limit
	response := &pc.Customers{}
	rows, err := r.Db.Query(`select 
			id,
			first_name,
			last_name,
			bio,
			email,
			phone_number
			from customers where deleted_at is null LIMIT $1 OFFSET $2`, limit, page)
	if err != nil {
		fmt.Println(err)
		return &pc.Customers{}, err
	}
	for rows.Next() {
		customer := &pc.CustomerRes{}
		err := rows.Scan(
			&customer.Id,
			&customer.FirstName,
			&customer.LastName,
			&customer.Bio,
			&customer.Email,
			&customer.PhoneNumber)
		if err != nil {
			fmt.Println(err)
			return &pc.Customers{}, err
		}
		rowAdd, err := r.Db.Query(`select id, street, district, user_id from addresses where user_id=$1`, customer.Id)
		if err != nil {
			fmt.Println(err)
			return &pc.Customers{}, err
		}
		for rowAdd.Next() {
			address := &pc.AddressRes{}
			err := rowAdd.Scan(
				&address.Id,
				&address.Street,
				&address.District,
				&address.UserId)
			if err != nil {
				fmt.Println(err)
				return &pc.Customers{}, err
			}
			customer.Addresses = append(customer.Addresses, address)
		}
		// fmt.Println(customer)
		response.Customers = append(response.Customers, customer)
	}
	if err != nil {
		fmt.Println(err)
		return &pc.Customers{}, err
	}
	// fmt.Println(response, "bosh keldi str psql")
	return response, nil
}

func (r *CustomerRepo) SearchCustomer(req *pc.SearchReq) (*pc.Customers, error) {
	key := req.Key
	value := req.Value
	limit := req.Limit
	page := (req.Page - 1) * limit
	query := fmt.Sprintf(`select id, first_name, last_name, bio, email, phone_number
	from customers where deleted_at is null and %s LIKE %s LIMIT %d OFFSET %d`, key, "%"+value+"%", limit, page)
	response := &pc.Customers{}
	rows, err := r.Db.Query(query, key, value)
	if err != nil {
		fmt.Println(err)
		return &pc.Customers{}, err
	}
	for rows.Next() {
		customer := &pc.CustomerRes{}
		err := rows.Scan(
			&customer.Id,
			&customer.FirstName,
			&customer.LastName,
			&customer.Bio,
			&customer.Email,
			&customer.PhoneNumber)
		if err != nil {
			fmt.Println(err)
			return &pc.Customers{}, err
		}
		rowAdd, err := r.Db.Query(`select id, street, district, user_id from addresses where user_id=$1`, customer.Id)
		if err != nil {
			fmt.Println(err)
			return &pc.Customers{}, err
		}
		for rowAdd.Next() {
			address := &pc.AddressRes{}
			err := rowAdd.Scan(
				&address.Id,
				&address.Street,
				&address.District,
				&address.UserId)
			if err != nil {
				fmt.Println(err)
				return &pc.Customers{}, err
			}
			customer.Addresses = append(customer.Addresses, address)
		}
		// fmt.Println(customer)
		response.Customers = append(response.Customers, customer)
	}
	if err != nil {
		fmt.Println(err)
		return &pc.Customers{}, err
	}
	// fmt.Println(response, "bosh keldi str psql")
	return response, nil
}

func (r *CustomerRepo) LogIn(req *pc.LoginRequest) (*pc.CustomerRes, error) {
	user := &pc.CustomerRes{}
	err := r.Db.QueryRow(`select 
	id, 
	first_name,
	last_name,
	bio,email,password,
	phone_number,refresh_token, created_at, updated_at from customers where email = $1`, req.Email).Scan(
		&user.Id,
		&user.FirstName,
		&user.LastName, &user.Bio,
		&user.Email,
		&user.Password, &user.PhoneNumber, &user.RefreshToken,
		&user.CreatedAt, &user.UpdatedAt)
	if err != nil {
		return &pc.CustomerRes{}, err
	}
	rowAdd, err := r.Db.Query(`select id,street,district,user_id from addresses where user_id=$1`, user.Id)
	if err != nil {
		return &pc.CustomerRes{}, err
	}
	defer rowAdd.Close()
	for rowAdd.Next() {
		address := &pc.AddressRes{}
		rowAdd.Scan(
			&address.Id,
			&address.Street,
			&address.District, &address.UserId)
		user.Addresses = append(user.Addresses, address)
	}
	return user, nil
}

func (r *CustomerRepo) GetCustomer(req *pc.CustomerId) (*pc.Customer, error) {
	user := &pc.Customer{}
	err := r.Db.QueryRow(`select 
	id, 
	first_name,
	last_name,
	bio,email,
	phone_number from customers where id = $1`, req.Id).Scan(
		&user.Id,
		&user.FirstName,
		&user.LastName, &user.Bio,
		&user.Email, &user.PhoneNumber)
	if err != nil {
		return &pc.Customer{}, err
	}
	rowAdd, err := r.Db.Query(`select id,street,district,user_id from addresses where user_id=$1`, req.Id)
	if err != nil {
		return &pc.Customer{}, err
	}
	defer rowAdd.Close()
	for rowAdd.Next() {
		address := &pc.AddressRes{}
		rowAdd.Scan(
			&address.Id,
			&address.Street,
			&address.District, &address.UserId)
		user.Addresses = append(user.Addresses, address)
	}
	return user, nil
}

func (r *CustomerRepo) UpdateCustomer(user *pc.CustomerRes) (*pc.CustomerRes, error) {
	customerResp := pc.CustomerRes{}
	err := r.Db.QueryRow(`UPDATE customers SET  updated_at=now(),
	first_name=$1,last_name=$2,bio=$3,
	email=$4,phone_number=$5 where id=$6 and deleted_at is null returning id,first_name,last_name,bio,email,phone_number`,
		user.FirstName, user.LastName, user.Bio, user.Email, user.PhoneNumber, user.Id).Scan(
		&customerResp.Id, &customerResp.FirstName, &customerResp.LastName,
		&customerResp.Bio, &customerResp.Email, &customerResp.PhoneNumber)
	if err != nil {
		fmt.Println(err)
		return &pc.CustomerRes{}, err
	}
	for _, address := range user.Addresses {
		addressResp := pc.AddressRes{}
		err = r.Db.QueryRow(`UPDATE addresses
		SET updated_at=now(), district=$1,street=$2 where user_id=$3 and deleted_at is null returning id,user_id,district,street`,
			address.District, address.Street, user.Id).Scan(
			&addressResp.Id, &addressResp.UserId, &addressResp.District, &addressResp.Street,
		)
		if err != nil {
			fmt.Println(err)
			return &pc.CustomerRes{}, err
		}
		customerResp.Addresses = append(customerResp.Addresses, &addressResp)
	}
	return &customerResp, nil

}

func (r *CustomerRepo) DeleteCustomer(id *pc.CustomerId) (*pc.Empty, error) {
	err := r.DeleteAddress(id)
	if err != nil {
		fmt.Println("error while deleting addresses")
		return &pc.Empty{}, err
	}
	_, err = r.Db.Exec(`UPDATE customers SET deleted_at=NOW() where id=$1 and deleted_at is null`, id.Id)
	if err != nil {
		fmt.Println("error while deleting customers")
		return &pc.Empty{}, err
	}
	return &pc.Empty{}, nil
}

func (r *CustomerRepo) UpdateAddressUserId(adds []*pc.AddressRes) error {
	for _, a := range adds {
		err := r.Db.QueryRow(`UPDATE addresses
		SET street=$1, district=$2, 
		updated_at=NOW() where user_id=$3`,
			a.Street, a.District, a.UserId).Err()
		if err != nil {
			fmt.Println(err)
			return err
		}
	}
	return nil
}
func (r *CustomerRepo) DeleteAddress(userId *pc.CustomerId) error {
	return r.Db.QueryRow(`UPDATE addresses SET deleted_at=NOW() where user_id=$1 and deleted_at is null`, userId.Id).Err()
}

func (r *CustomerRepo) CheckField(req *pc.CheckFieldReq) (*pc.CheckFieldRes, error) {
	query := fmt.Sprintf("SELECT 1 FROM customers WHERE %s=$1", req.Field)
	res := &pc.CheckFieldRes{}
	temp := -1
	err := r.Db.QueryRow(query, req.Value).Scan(&temp)

	if err != nil {
		res.Exists = false
		return res, nil
	}
	if temp == 0 {
		res.Exists = true
	} else {
		res.Exists = false
	}
	return res, nil
}
