package storage

import (
	"customer-service/storage/postgres"
	"customer-service/storage/repo"

	"github.com/jmoiron/sqlx"
)

type IStorage interface {
	Customer() repo.CustomerStorageI
}

type StoragePg struct {
	Db       *sqlx.DB
	customerRepo repo.CustomerStorageI
}

func NewStoragePg(db *sqlx.DB) *StoragePg {
	return &StoragePg{
		Db:       db,
		customerRepo: postgres.NewCustomerRepo(db),
	}
}

func (s *StoragePg) Customer() repo.CustomerStorageI {
	return s.customerRepo
}
