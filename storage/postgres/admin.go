package postgres

import (
	pb "customer-service/genproto/customer"
	"database/sql"
	"fmt"
)

func (r *CustomerRepo) GetAdmin(req *pb.AdminCutomerReq) (*pb.AdminCustomerRes, error) {
	res := pb.AdminCustomerRes{}
	err := r.Db.QueryRow(`SELECT 
		id,
		admin_name,
		admin_password, 
		email,
		created_at, 
		updated_at
		FROM admins 
		WHERE deleted_at 
		IS NULL AND 
		admin_name=$1`, req.Name).
		Scan(&res.Id, &res.Name, &res.Password, &res.Email, &res.CreatedAt, &res.UpdatedAt)
	if err == sql.ErrNoRows {
		fmt.Println("No rows")
		return &res, nil
	}
	if err != nil {
		return &pb.AdminCustomerRes{}, err
	}
	return &res, nil
}
