package repo

import (
	pb "customer-service/genproto/customer"
)

type CustomerStorageI interface {
	CreateCustomer(*pb.CustomerReq) (*pb.CustomerRes, error)
	GetCustomer(*pb.CustomerId) (*pb.Customer, error)
	UpdateCustomer(*pb.CustomerRes) (*pb.CustomerRes, error)
	GetCustomers(*pb.Empty) (*pb.Customers, error)
	DeleteCustomer(*pb.CustomerId) (*pb.Empty, error)
	CheckField(req *pb.CheckFieldReq) (*pb.CheckFieldRes, error)
	ListCustomers(req *pb.ListUserReq) (*pb.Customers, error)
	SearchCustomer(req *pb.SearchReq) (*pb.Customers, error)
	LogIn(req *pb.LoginRequest) (*pb.CustomerRes, error)
	GetAdmin(req *pb.AdminCutomerReq) (*pb.AdminCustomerRes, error)
	GetModerator(*pb.GetModeratorReq) (*pb.GetModeratorRes, error)
}

/*
rpc CreateCustomer(CustomerReq) returns (CustomerReq);
rpc GetCutomer (CustomerId) returns (Customer);
rpc GetCustomers(CostumersIds) returns (Customers);
rpc UpdateCustomer(CustomerReq) returns (CustomerReq);
rpc DeleteCustomer(CustomerId) returns (Empty);

*/
