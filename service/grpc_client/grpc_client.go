package grpcclient

import (
	"customer-service/config"
	ps "customer-service/genproto/post"
	rs "customer-service/genproto/review"
	"fmt"

	"google.golang.org/grpc"
)
// connect to client
type GrpcClientI interface {
	Post() ps.PostServiceClient
	Review() rs.ReviewServiceClient
}

type GrpcClient struct {
	cfg           config.Config
	postService   ps.PostServiceClient
	reviewService rs.ReviewServiceClient
}

func New(cfg config.Config) (*GrpcClient, error) {
	connReview, err := grpc.Dial(
		fmt.Sprintf("%s:%d", cfg.ReviewServiceHost, cfg.ReviewServicePort),
		grpc.WithInsecure())
	if err != nil {
		return nil, fmt.Errorf("review service dial host:%s, port: %d", cfg.ReviewServiceHost, cfg.ReviewServicePort)
	}

	connPost, err := grpc.Dial(
		fmt.Sprintf("%s:%d", cfg.PostServiceHost, cfg.PostServicePort),
		grpc.WithInsecure())
		
	if err != nil {
		return nil, fmt.Errorf("post service dial host:%s, port: %d", cfg.PostServiceHost, cfg.PostServicePort)
	}
	return &GrpcClient{
		cfg:           cfg,
		postService:   ps.NewPostServiceClient(connPost),
		reviewService: rs.NewReviewServiceClient(connReview),
	}, nil
}

func (s *GrpcClient) Review() rs.ReviewServiceClient {
	return s.reviewService
}
func (s *GrpcClient) Post() ps.PostServiceClient {
	return s.postService
}
