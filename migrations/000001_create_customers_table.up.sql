CREATE TABLE IF NOT EXISTS customers(
    id uuid NOT NULL primary key,
    first_name varchar(50),
    last_name varchar(50),
    bio text,
    password text NOT NULL,
    refresh_token text, 
    email varchar(50),
    phone_number varchar(35),
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    deleted_at TIMESTAMP
);